const express = require('express')
const app = express();
const port = 8000;
require('./db/Connection');
const serviceRoute = require('./route/Service');
const cors = require('cors');

app.use(cors())
app.use(express.json());
app.use(serviceRoute);

app.listen(port, function(){
    console.log("App Start in Port " + port);
})