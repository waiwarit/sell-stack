const mongoose = require('mongoose');
const moment = require('moment');
const request = require('request');

var SellSchema = new mongoose.Schema({
    sell_date:{
        type:String,
        required:true,
    },
    type_product:{
        type:String,
        required:true,
        validate(value){
            var product = ["Drinks", "Fresh-food", "Dry-food"]
            if(!product.includes(value)){
                throw new Error("Product have Drinks, Fresh-food and Dry-food");
            }
        }
    },
    sell_number:{
        type:Number,
        required:true
    }
});

module.exports = mongoose.model('Sell', SellSchema);