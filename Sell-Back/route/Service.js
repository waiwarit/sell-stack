const express = require('express');
const router = express.Router();
const mySell = require('../db/ListSell.model');
const moment = require('moment');

router.get("/", (req, res) => {
    res.send('Welcome to Sell Stack');
});

//Add you Item
router.post("/sell/add", async (req, res) => {
    try {
        const payload = req.body;
        validatefForm(payload.type_product, payload.sell_number);                   //Validate request form by function
        let checkSell = await mySell.findOne({ sell_date: payload.sell_date }).where('type_product').equals(payload.type_product);     
        if (checkSell == null) {                                                    //This line is check sell_date or have in database or not
            const sell = new mySell(payload);
            await sell.save();
            res.status(201).send({ message: sell });
        } else {
            await mySell.findByIdAndUpdate(checkSell._id, {sell_number:checkSell.sell_number+payload.sell_number}); //if have it will sum sell_number and save in database
            res.status(201).send({message:"Update the same date"});
        }
    } catch (e) {
        res.status(400).send({ message: e.message });
    }
});

//View you all item
router.get("/sell", async (req, res) => {
    const sells = await mySell.find({});
    res.send(sells);
});

//View you item by ID
router.get('/sell/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const sell = await mySell.findById(_id);
        res.send(sell);
    } catch (e) {
        res.status(404).send("Id not found");
    }
});

//Update your item in you want by ID
router.patch("/sell/update/:id", async (req, res) => {
    const _id = req.params.id;
    const payload = req.body;
    try {
        validatefForm(payload.type_product, payload.sell_number);                               //valite form before update
        await mySell.findByIdAndUpdate(_id, { $set: payload }, function (err, result) {
            if (err) {
                console.log(err);
            }
            console.log("RESULT: " + result);
            res.send({ message: "Update id: " + _id + " sucess" });
        })
    } catch (e) {
        res.status(400).send({ message: e.message });
    }
});

//Delete your item in you want by ID 
router.delete('/sell/delete/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const sell = await mySell.findByIdAndDelete(_id);
        res.send({ message: "delete " + sell.type_product });
    } catch (e) {
        res.status(404).send("id not found");
    }
});

//Get item in product you want
router.get('/sell/product/:product', async (req, res) => {
    const product = req.params.product;
    try {
        const sell = await mySell.find({ type_product: product })
        res.send(sell)
    } catch (e) {
        res.status(404).send(e)
    }
});

//Filter Better Date

router.post('/sell/filter', async(req, res) =>{
    const start_date = req.body.start_date;
    const end_date = req.body.end_date;
    try{
        const sellItem = await mySell.find({}).where('sell_date').gte(start_date).lte(end_date);
        res.status(201).send({message:sellItem});
    }catch(e){
        res.send(400).send({message:e.message});
    }
});

//This function use for validate request form
function validatefForm(type_product, sell_number) {
    var product = ["Drinks", "Fresh-food", "Dry-food"];
    if (!product.includes(type_product)) {
        throw new Error("Product have Drinks, Fresh-food and Dry-food");
    }
    if (sell_number == null) {
        throw new Error("sale_number is require;");
    }
};

module.exports = router;