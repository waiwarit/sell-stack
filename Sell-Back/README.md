# Sell-Stack Service  
	เป็น Service ที่ให้บริการสำหรับเก็บยอดขาย และ สามารถเปรียบเทียบยอดขายแต่ละชิ้นกันได้   
---
## Usage  
  *** URL : `localhost:8000/sell/add` ****  
  ** Method : `POST`  
  **Example Request :  **  
```json
      {  
        "sell_date" : "2014-12-13 12:34 PM",
		"type_product" : "Drinks",
		"sell_number" : 40
      }
```
  ** Example Response : **
```json
      {
    	"message": {
        				"_id": "5e27037a08677d1a6cae57c7",
        				"sell_date": "2020-01-11 12:00 PM",
        				"type_product": "Drinks",
						"sell_number": 40,
        				"__v": 0
    				}
	  }
```
---
  *** URL : `localhost:8000/sell` ****  
  ** Method : `GET`  
  ** Example Respond **  
```json
      [
        {
          "_id": "5e27037a08677d1a6cae57c7",
          "sell_date": "2020-01-11 12:00 PM",
          "type_product": "Drinks",
          "sell_number": 40,
          "__v": 0
        },
        {
          "_id": "5e27058b08677d1a6cae57c8",
          "sell_date": "2020-01-11 12:00 PM",
          "type_product": "fresh_food",
          "sell_number": 40,
          "__v": 0
        }
      ]
```
---
  *** URL : `localhost:8000/sell/id`  ***  
  ** Method : `GET`  
  ** Example Response **  
```json
    {
      "_id": "5e27035ace07c81a64f2f2b3",
      "sell_date": "2020-01-11 12:00 PM",
      "type_product": "Drinks",
      "sell_number": 40,
      "__v": 0
    }
```
---
  *** URL : `localhost:8000/sell/update/id` ***  
  ** Method : `PATCH`  
  ** Example Respond **  
```json
    {
      "message": "Update id: 5e27035ace07c81a64f2f2b3 sucess"
    }
```
---
 *** URL : `localhost:8000/sell/delete/id` ***  
 ** Method : `DELETE`  
 ** Example Response **  
```json
     {
        "message": "delete fresh_food"
     }
```
---  
 *** URL : `localhost:8000/sell/product/:product` ***  
 ** Method : `GET`  
 ** Example Request **  
    `localhost:8000/sell/product/Fresh-food`  
 ** Example Response **
```json
     {
         "message" : [
                        {
                          "_id": "5e3af04e8f63041dfc11d04e",
                          "sell_date": "2014-12-03 12:34 PM",
                          "type_product": "Fresh food",
                          "sell_number": 160,
                          "__v": 0
                        },
                        {
                          "_id": "5e3af1061d78b81e3ed0c55e",
                          "sell_date": "2014-12-01 12:34 PM",
                          "type_product": "Fresh food",
                          "sell_number": 60,
                          "__v": 0
                        }
                    ]
    }
```
---
  *** URL : `localhost:8000/sell/filter` ***
  ** Method : `POST`
  ** Example Request **
```json
    {
      "start_date" : "2014-12-01 12:34 PM",
      "end_date" : "2014-12-03 12:34 PM"
    }
```
  ** Example Response **
```json
    {
      "message" : [
                        {
                          "_id": "5e3af04e8f63041dfc11d04e",
                          "sell_date": "2014-12-03 12:34 PM",
                          "type_product": "Fresh food",
                          "sell_number": 160,
                          "__v": 0
                        },
                        {
                          "_id": "5e3af1061d78b81e3ed0c55e",
                          "sell_date": "2014-12-01 12:34 PM",
                          "type_product": "Fresh food",
                          "sell_number": 60,
                          "__v": 0
                        }
                    ]
    }
```