import Link from 'next/link'
import Head from 'next/head'

const styleButton = {
  backgroundColor: 'transparent',
  border: 'none',
  color: 'white',
  padding: '10px 10px',
  textAlign: 'center',
  textDecoration: 'none',
  display: 'inline-block',
  fontSize: '22px',
  margin: '4px 10px',
  cursor: 'pointer',
}

const Navbar = () => (
    <div style={{
        fontFamily: 'Roboto',
        fontWeight: 100,
        width: '100%',
        position: "absolute",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }}>
        <Head>
            <link href="https://fonts.googleapis.com/css?family=Orbitron|Roboto:100,400&display=swap" rel="stylesheet" />
        </Head>
        <Link href="/">
            <a style={styleButton}>Home</a>
        </Link>
        <Link href="/sell">
            <a style={styleButton}>Sell</a>
        </Link>
        <Link href="/">
            <a style={{
                width: 90,
                backgroundColor: 'transparent',
                border: 'none',
                padding: '10px 10px',
                textAlign: 'center',
                textDecoration: 'none',
                display: 'inline-block',
                margin: '4px 2px',
                cursor: 'pointer',
            }}>
                <img src='../static/logos/logo.png' width='100%' />
            </a>
        </Link>
        <Link href="/stats">
            <a style={styleButton}>Stats</a>
        </Link>
        <Link href="/about">
            <a style={styleButton}>About</a>
        </Link>
    </div>
)

export default Navbar