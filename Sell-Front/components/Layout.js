import Navbar from './Navbar'
import Footer from './Footer'

const Layout = (props) => (
    <div>
        <Navbar />
            <div style={{
                minHeight: '93vh'
            }}>
                {props.children}
            </div>
        <Footer />
        <style jsx global>{`
            @font-face {
                font-family: 'Abril';
                src: url('../static/fonts/Abril_Fatface/AbrilFatface-Regular.otf');
            }
            body {
                font-family: 'Abril';
                margin: 0;
                padding: 0;
                background-color: black;
                font-family: 'Prata', serif;
            }
        `}</style>
    </div>
)

export default Layout