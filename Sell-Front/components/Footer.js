const style = {
    right: 0,
    bottom: 0,
    left: 0,
    padding: '1rem',
    backgroundColor: 'white',
    textAlign: 'center',
    color: 'black',
  }
  
  const Footer = () => (
    <div style={style}>
      This is Footer.
    </div>
  )
  
  export default Footer