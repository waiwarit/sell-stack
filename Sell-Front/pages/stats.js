import { render } from "react-dom"
import React, { Component } from 'react'
import { Chart } from "react-google-charts"
import Layout from "../components/Layout"
import Head from 'next/head'

export default class Stats extends Component {
    static async getInitialProps () {
        const res = await fetch('http://localhost:8000/sell');
        const rawdata = await res.json();
        var data = [["Date", "Drinks", "Dry Food"]]
        var list = []

        for (var i = 0; i < rawdata.length; i++ ) {
            list = [rawdata[i].sell_date, rawdata[i].sell_number, i*3+10]
            data = data.concat([list])
            list = []
        }

        return { data }
    }

    constructor(props) {
        super(props)

        this.state = {
            options: {
                title: "Amount per day",
                hAxis: { title: "Date", viewWindow: { min: 0, max: 15 } },
                vAxis: { title: "Amount", viewWindow: { min: 0, max: 100 } },
                legend: "none",
            },
            data: props.data
        }
        console.log(this.state.data)
    }

    render(){
        return (
            <div>
                <Head>
                    <title>Home Page</title>
                    <link href="https://fonts.googleapis.com/css?family=Orbitron|Roboto:100,400&display=swap" rel="stylesheet" />
                </Head>
                <Layout>
                        <Chart
                            chartType="LineChart"
                            data={this.state.data}
                            options={this.state.options}
                            width="80%"
                            height="500px"
                            legendToggle
                            style={{
                                fontFamily: 'Orbitron',
                                paddingTop: 50,
                                paddingLeft: 150,
                                paddingBottom: 100
                            }}
                        />
                </Layout>
            </div>
        )
    }
}
