import Layout from '../components/Layout'
import Head from 'next/head'
import Link from 'next/link'

const styleLink = {
    marginTop: '70px',
    marginLeft: '70px',
    fontSize: 35,
}

const Index = () => (
    <div>
        <Head>
            <title>Home Page</title>
            <link href="https://fonts.googleapis.com/css?family=Orbitron|Roboto:100,400&display=swap" rel="stylesheet" />
        </Head>
        <Layout>
            <div style={{
                height: '100vh',
                fontFamily: 'Orbitron',
                backgroundColor: 'rgba(20,20,20)',
                color: 'white',
            }}>
                <div style={{
                    paddingTop: 150,
                    paddingLeft: 75
                }}>
                    <span style={{
                        fontSize: 50,
                    }}>
                        <span style={{color: 'rgb(57,255,20)'}}>>></span> . . / Sell & Stats<span className="blink">|</span>
                    </span>
                    <p style={styleLink}>Sell -> <Link href='/sell'><a>Sell</a></Link></p>
                    <p style={styleLink}>Statistics -> <Link href='/stats'><a>View Statistics</a></Link></p>
                    <p style={styleLink}>Need more detail? -> <Link href='/about'><a>About Us</a></Link></p>
                </div>
            </div>
        </Layout>
        <style jsx global>{`
            .blink {
                animation: blinker 1s step-start infinite;
            }
            
            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }
        `}</style>
    </div>
)
  
export default Index