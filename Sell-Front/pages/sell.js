import React, { Component } from 'react'
import axios from 'axios'
import Layout from '../components/Layout'
import Head from 'next/head'

export default class Sell extends Component {

    constructor(props) {
        super(props)

        this.onChangeDatetime = this.onChangeDatetime.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeID = this.onChangeID.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onDelete = this.onDelete.bind(this);

        this.state = {
            datetime: '',
            type: '',
            amount: '',
            date: '',
            hour: '',
            minute: '',
            time: '',
            id: '',
        }
    }

    onChangeDatetime(e) {
        this.setState({ datetime: e.target.value })

        this.setState({ date: e.target.value.slice(0, 10) })

        this.setState({ minute: e.target.value.slice(14) })

        if (parseInt(e.target.value.slice(11,13)) >= 13) {
            this.setState({ 
                hour: parseInt(e.target.value.slice(11,13)) - 12 + '',
                time: 'PM'
            })
        } else if (parseInt(e.target.value.slice(11,13)) === 0 ) {
            this.setState({
                hour: '12',
                time: 'AM'
            })
        } else if (parseInt(e.target.value.slice(11,13)) === 12) {
            this.setState({
                hour: '12',
                time: 'PM'
            })
        } else {
            this.setState({
                hour: e.target.value.slice(11,13) + '',
                time: 'AM'})
        }
    }

    onChangeType(e) {
        this.setState({ type: e.target.value })
    }

    onChangeAmount(e) {
        this.setState({ amount: e.target.value })
    }

    onChangeID(e) {
        this.setState({ id: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const userObject = {
            "sell_date" : this.state.date + ' ' + this.state.hour  + ':' + this.state.minute + ' ' + this.state.time,
             "type_product" : this.state.type,
             "sell_number" : this.state.amount
        };

        axios.post('http://localhost:8000/sell/add', userObject)
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });

        this.setState({ datetime: '', type: '', amount: '',hour: '', date: '', minute: '', time: '' })
    }

    onDelete(e) {
        e.preventDefault()

        const userObject = {
            "_id" : this.state.id
        };

        axios.delete('http://localhost:8000/sell/delete/'+this.state.id, userObject)
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });

        this.setState({ id: ''})
    }


    render() {
        return (
            <div>
                <Head>
                    <title>Sell Page</title>
                    <link href="https://fonts.googleapis.com/css?family=Orbitron|Roboto:100,400&display=swap" rel="stylesheet" />
                </Head>
                <Layout>
                    <div style={{marginLeft: 400, fontFamily: 'Orbitron',}}>
                        <form onSubmit={this.onSubmit} style={{color: 'white', paddingTop: '100px'}}>
                            <h1>Add</h1>
                            <div className="form-group">
                                <label>Datetime</label>
                                <input type="datetime-local" id="meeting-time" onChange={this.onChangeDatetime} className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Type</label>
                                <input type="text" onChange={this.onChangeType} className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Amount</label>
                                <input type="number" min="0" onChange={this.onChangeAmount} className="form-control" />
                            </div>
                            <div className="form-group">
                                <input type="submit" value="Create User" className="btn btn-success btn-block" />
                            </div>
                        </form>
                        <form onSubmit={this.onDelete} style={{color: 'white', marginTop: '100px'}}>
                            <h1>Delete</h1>
                            <div className="form-group">
                                <label>ID</label>
                                <input type="text" onChange={this.onChangeID} className="form-control" />
                            </div>
                            <div className="form-group">
                                <input type="submit" value="Delete" className="btn btn-success btn-block" />
                            </div>
                        </form>
                    </div>
                    <style jsx global>{`
                        @font-face {
                            font-family: 'Abril';
                            src: url('../static/fonts/Abril_Fatface/AbrilFatface-Regular.otf');
                        }
                        label {
                            margin-right: 20px; 
                        }
                        .btn {
                            margin-top: 20px;
                        }
                    `}</style>
                </Layout>
            </div>
        )
    }
}